import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MapLocation } from '../models/location.model';
import * as d3 from 'd3';
import * as d3Shape from 'd3-shape';

@Component({
    selector: 'app-lottery',
    templateUrl: './lottery.component.html',
    styleUrls: ['./lottery.component.scss']
})
export class LotteryComponent implements OnInit {
    private width: number;
    private height: number;
    private radius: number;
    private step: number;

    private arc: any;
    private pie: any;
    private color: any;
    private wheel: any;
    private svg: any;

    private isRotating = false;

    private rotation: number;
    private oldRotation = 0;

    @Output() closeDialog: EventEmitter<void> = new EventEmitter();
    @Output() winningLocation: EventEmitter<MapLocation> = new EventEmitter();
    @Input() locations: MapLocation[];

    constructor() {}

    ngOnInit() {
        this.width = 500;
        this.height = 500;
        this.radius = Math.min(this.width, this.height) / 2 - 10;
        this.initSvg();
        this.drawPie();
    }

    onClose() {
        this.closeDialog.emit();
    }

    private initSvg() {
        // Number of steps/options in wheel.
        this.step = 360 / this.locations.length;
        // Generate colors array.
        this.color = d3.scaleOrdinal(d3.schemeCategory10);
        this.arc = d3Shape.arc()
            .outerRadius(this.radius)
            .innerRadius(0);
        this.pie = d3Shape.pie()
            .sort(null)
            .value((d: any) => this.step);
        this.svg = d3.select('svg')
            .append('g')
            .attr('transform', `translate(${this.width / 2}, ${this.height / 2})`);
    }

    private drawPie() {
        // Create group inside svg to group all pie parts.
        this.wheel = this.svg.append('g').attr('class', 'wheel');
        // Create the pie.
        const group = this.wheel.selectAll('.arc')
            .data(this.pie(this.locations))
            .enter().append('g')
            .attr('class', 'arc');
        group.append('path').attr('d', this.arc)
            .style('fill', (d, i) => this.color(i));
        // Add legends.
        group.append('text').attr('transform', d => {
            d.innerRadius = 0;
            d.outerRadius = this.radius;
            d.angle = (d.startAngle + d.endAngle) / 2;
            return `rotate(${(d.angle * 180 / Math.PI - 90)})translate(${(d.outerRadius - 10)})`;
        })
            .attr('text-anchor', 'end')
            .text(d => {
                return d.data.name;
            });

        // arrow
        this.svg.append('svg:defs').append('svg:marker')
            .attr('id', 'triangle')
            .attr('refX', 6)
            .attr('refY', 6)
            .attr('markerWidth', 30)
            .attr('markerHeight', 30)
            .attr('markerUnits', 'userSpaceOnUse')
            .attr('orient', 'auto')
            .append('path')
            .attr('d', 'M 0 0 12 6 0 12 3 6')
            .style('fill', 'red');
        // line
        this.svg.append('line')
            .attr('x1', 0)
            .attr('y1', 0)
            .attr('x2', 0)
            .attr('y2', this.radius * -1 + 100)
            .attr('stroke-width', 5)
            .attr('stroke', 'red')
            .attr('marker-end', 'url(#triangle)')
            .attr('transform', `rotate(${this.step / 2})`);
    }

    spin() {
        if (!this.isRotating) {
            this.isRotating = true;
            // Create random range for the spin (spin amount).
            const rng = Math.floor((Math.random() * 1440) + 360);
            this.rotation = (Math.round(rng / this.step) * this.step);
            // Calculate wich location is going to win;
            let picked = Math.round(this.locations.length - (this.rotation % 360) / this.step);
            picked = picked >= this.locations.length ? (picked % this.locations.length) : picked;
            // Add half step to rotatin so the arrow will be in the middle.
            this.rotation += this.step / 2 - Math.round(this.step / 2);
            //  Handle animation.
            this.wheel.transition()
                .duration(3000)
                .attrTween('transform', this.rotTween)
                .on('end', () => {
                    // When rotate animation ends.
                    this.oldRotation = this.rotation;
                    setTimeout(() => {
                        this.winningLocation.emit(this.locations[picked]);
                        this.closeDialog.emit();
                    }, 1000);
                });
        }
    }

    private rotTween = () => {
        const i = d3.interpolate(this.oldRotation % 360, this.rotation);
        return (t) => {
            return `rotate(${i(t)})`;
        };
    }
}
