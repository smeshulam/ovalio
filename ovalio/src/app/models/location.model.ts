export class MapLocation {
  constructor(public name: string, public latitude: number, public longitude: number) {}
}
