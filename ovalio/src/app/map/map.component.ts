/// <reference types="@types/googlemaps" />
import { MapsAPILoader } from '@agm/core';
import { Component, OnInit, ViewChild, ElementRef, NgZone, OnDestroy } from '@angular/core';
import { MapLocation } from '../models/location.model';
import { MapService } from '../services/map.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {
  latitude = 31.046051;
  longitude = 34.851612;
  zoom = 8;
  locations: MapLocation[];

  dialogVisible = false;

  @ViewChild('searchInput', { static: false }) searchInputRef: ElementRef;

  private subscription: Subscription;

  constructor(
    private mapService: MapService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { }

  ngOnInit() {
    // Get All initial locations from service.
    this.locations = this.mapService.getLocations();
    // Subscribe to locations change.
    this.subscription = this.mapService.locationAdded.subscribe(locations => this.locations = locations);
    // Handles autocomplete -> logic happens after gmap api is loaded.
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchInputRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();
          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          // Add to store
          const mapLocation = new MapLocation(
              place.name,
              place.geometry.location.lat(),
              place.geometry.location.lng()
          );
          this.mapService.addLocation(mapLocation);
        });
      });
    });
  }

  toggleDialog() {
    this.dialogVisible = !this.dialogVisible;
  }

  onLotteryResult(l: MapLocation) {
    this.latitude = l.latitude;
    this.longitude = l.longitude;
    this.zoom = 10;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
