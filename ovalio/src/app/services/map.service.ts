import { Injectable } from '@angular/core';
import { MapLocation } from '../models/location.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  locationAdded: Subject<MapLocation[]> = new Subject();

  private locations: MapLocation[] = [
    new MapLocation('Tel-Aviv', 32.085300, 34.781769),
    new MapLocation('Ashkelon', 31.674891, 34.575039),
    new MapLocation('Beer-Sehva', 31.243870, 34.793991),
    new MapLocation('Hifa', 32.817280, 34.988760),
    new MapLocation('Eilat', 29.550360, 34.952278)
  ];

  constructor() { }

  getLocations(): MapLocation[] {
    return [...this.locations];
  }

  addLocation(l: MapLocation) {
    this.locations.push(l);
    this.locationAdded.next(this.getLocations());
  }
}
